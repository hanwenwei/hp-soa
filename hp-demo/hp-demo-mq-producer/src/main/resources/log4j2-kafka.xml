<?xml version="1.0" encoding="UTF-8"?>
<Configuration  packages="io.github.hpsocket.soa.framework.core.log" shutdownHook="disable" strict="true">

    <Properties>
        <Property name="base.package">${project.groupId}</Property>
        <Property name="logger.dao.pattern">com\.github\.hpsocket\.demo\..*\.dao\..+</Property>
        <Property name="log.include.location">${sys:log4j.include.location:-true}</Property>
        <Property name="log.level">${sys:log4j.log.level:-DEBUG}</Property>
        <Property name="kafka.bootstrap.servers">${sys:log4j.kafka.bootstrap.servers:-192.168.56.23:9092}</Property>
        <Property name="kafka.security.protocol">${sys:log4j.security.protocol:-PLAINTEXT}</Property>
        <Property name="kafka.sasl.mechanism">${sys:log4j.sasl.mechanism:-PLAIN}</Property>
        <Property name="kafka.sasl.jaas.config">${sys:log4j.kafka.sasl.jaas.config:-}</Property>
        <Property name="kafka.topic">${sys:log4j.kafka.topic:-hp-soa}</Property>
        <Property name="kafka.logType">${sys:log4j.kafka.logType:-hp-soa}</Property>
        <Property name="kafka.ignoreExceptions">${sys:log4j.kafka.ignoreExceptions:-true}</Property>
        <Property name="kafka.syncSend">${sys:log4j.kafka.syncSend:-true}</Property>
        <Property name="logfile.path">${sys:log4j.logfile.path:-/data/logs/access}/${project.artifactId}</Property>
        <Property name="logfile.max.size">100 MB</Property>
        <Property name="logfile.max.count">10</Property>
        <Property name="logfile.archive.path">${sys:log4j.logfile.path:-/data/logs/access}/${project.artifactId}/${date:yyyy-MM}</Property>
        <Property name="logfile.archive.suffix">%d{yyyy-MM-dd}-%i.log.gz</Property>
        <Property name="logfile.stacktrace.filters">org.springframework.security.web.ObservationFilterChainDecorator,org.apache.catalina,org.apache.coyote,org.apache.tomcat,org.hibernate,org.apache.el,org.apache.cxf,org.junit,junit.framework,org.jboss,org.h2,org.eclipse,org.richfaces,java.lang.reflect,java.base/java.lang.reflect,jdk.internal.reflect,java.base/jdk.internal.reflect,com.sun,javax.servlet,jakarta.servlet</Property>
        <Property name="logfile.pattern">%d{yyyy-MM-dd HH:mm:ss.SSS} %-5p [%t] %C{1.1.1.*}#%M\(L:%L\) -&gt; %m%xEx{filters(${logfile.stacktrace.filters})}%n</Property>
        <Property name="json.template.layout">classpath:log4j2-default-template.json</Property>
    </Properties>

    <Appenders>
        <Console name="STDOUT" target="SYSTEM_OUT">
            <PatternLayout pattern="${logfile.pattern}"/>
        </Console>
        <Console name="STDERR" target="SYSTEM_ERR">
            <PatternLayout pattern="${logfile.pattern}"/>
        </Console>
        
        <RollingRandomAccessFile name="MONITOR_LOG_FILE" fileName="${logfile.path}/monitor.log"
                                 filePattern="${logfile.archive.path}/monitor-${logfile.archive.suffix}">
            <PatternLayout pattern="${logfile.pattern}"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="${logfile.max.size}"/>
            </Policies>
            <DefaultRolloverStrategy max="${logfile.max.count}"/>
        </RollingRandomAccessFile>
        <RollingRandomAccessFile name="SERVICE_LOG_FILE" fileName="${logfile.path}/service.log"
                                 filePattern="${logfile.archive.path}/service-${logfile.archive.suffix}">
            <PatternLayout pattern="${logfile.pattern}"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="${logfile.max.size}"/>
            </Policies>
            <DefaultRolloverStrategy max="${logfile.max.count}"/>
        </RollingRandomAccessFile>
        
        <kafka name="KAFKA_MONITOR" topic="${kafka.topic}"
            ignoreExceptions="${kafka.ignoreExceptions}" syncSend="${kafka.syncSend}">
            <Property name="bootstrap.servers">${kafka.bootstrap.servers}</Property>
            <Property name="security.protocol">${kafka.security.protocol}</Property>
            <Property name="sasl.mechanism">${kafka.sasl.mechanism}</Property>
            <Property name="sasl.jaas.config">${kafka.sasl.jaas.config}</Property>
            <JsonTemplateLayout eventTemplateUri="${json.template.layout}">
                <EventTemplateAdditionalField key="log_type" value="${kafka.logType}"/>
                <EventTemplateAdditionalField key="facility" value="SOA-MONITOR"/>
                <EventTemplateAdditionalField key="@version" value="${project.version}"/>
            </JsonTemplateLayout>
        </kafka>

        <kafka name="KAFKA_SERVICE" topic="${kafka.topic}"
            ignoreExceptions="${kafka.ignoreExceptions}" syncSend="${kafka.syncSend}">
            <Property name="bootstrap.servers">${kafka.bootstrap.servers}</Property>
            <Property name="security.protocol">${kafka.security.protocol}</Property>
            <Property name="sasl.mechanism">${kafka.sasl.mechanism}</Property>
            <Property name="sasl.jaas.config">${kafka.sasl.jaas.config}</Property>
            <JsonTemplateLayout eventTemplateUri="${json.template.layout}">
                <EventTemplateAdditionalField key="log_type" value="${kafka.logType}"/>
                <EventTemplateAdditionalField key="facility" value="SOA-SERVICE"/>
                <EventTemplateAdditionalField key="@version" value="${project.version}"/>
            </JsonTemplateLayout>
        </kafka>
    </Appenders>

    <Loggers>
        <!-- Root Logger -->
        <Root level="INFO" includeLocation="${log.include.location}">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Root>
        <!-- Service Logger -->
        <Logger name="${base.package}" level="${log.level}" includeLocation="${log.include.location}" additivity="false">
            <LoggerNameFilter name="${logger.dao.pattern}" level="WARN" onMatch="ACCEPT" onMismatch="DENY"/>
            <AppenderRef ref="STDERR"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
        <!-- SOA-MONITOR Logger -->
        <Logger name="SOA-MONITOR" level="${log.level}" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="MONITOR_LOG_FILE"/>
            <AppenderRef ref="KAFKA_MONITOR"/>
        </Logger>
        <!-- HP-SOA Logger -->
        <Logger name="io.github.hpsocket" level="${log.level}" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
        <!-- Dubbo Logger -->
        <Logger name="org.apache.dubbo" level="INFO" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
        <!-- XXL-JOB Logger -->
        <Logger name="com.xxl.job" level="INFO" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
        <!-- Kafka Logger -->
        <Logger name="org.apache.kafka" level="INFO" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
        <!-- MyBatis Logger -->
        <Logger name="org.apache.ibatis" level="WARN" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
        <!-- JDBC Logger -->
        <Logger name="java.sql" level="WARN" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
        <!-- Spring JDBC Logger -->
        <Logger name="org.springframework.jdbc" level="WARN" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
        <!-- MongoDB -->
        <Logger name="com.mongodb" level="WARN" includeLocation="${log.include.location}" additivity="false">
            <AppenderRef ref="STDOUT"/>
            <AppenderRef ref="SERVICE_LOG_FILE"/>
            <AppenderRef ref="KAFKA_SERVICE"/>
        </Logger>
    </Loggers>

</Configuration>
