package io.github.hpsocket.demo.infra.mysql.config;

import org.springframework.boot.autoconfigure.AutoConfiguration;

@AutoConfiguration
/* default mybatis mapper scan package -> ${hp.soa.web.mapper-scan.base-package} */
//@MapperScan("io.github.hpsocket.demo.infra.mysql.mapper")
public class AppConfig
{

}
